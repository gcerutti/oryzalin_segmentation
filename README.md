# Oryzalin Segmentation

Notebooks and scripts to facilitate the segmentation of 3D confocal images NPA + Oryzalin treated meristems.

### Requirements

If necessary, create a `conda` environment with the required dependencies as follows:

```bash
conda create -n timagetk -c mosaic -c morpheme -c conda-forge timagetk pyvista pandas notebook
```

Then, to run the notebooks in the `notebooks` folder, run:

```bash
conda activate timagetk
jupyter notebook
```
